gfx = { };

var loader, images;

images = [
    "gfx/actionPrompt.png",
    "gfx/androidBtnA.png"
];

loader = new PIXI.AssetLoader(images);

// Execute the loading function
loader.onComplete = game.load;

loader.onProgress = function (x) {
    var name = x.texture.baseTexture.imageUrl;
    name = name.slice(4, -4);
    gfx[name] = x.texture;
};

loader.load();
