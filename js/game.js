// chikun :: 2014
// Contains all game functions


// Container for all game functions
var game = { };
var newSprite;


// Performed at game load
game.load = function () {

    // Create a new instance of the PIXI stage
    stage = new PIXI.Stage(0xA20E7A);

    // Create a renderer instance
    renderer = new PIXI.autoDetectRenderer(854, 480);

    // Attach the renderer to the container div
    document.getElementById("container").appendChild(renderer.view);

    requestAnimFrame(game.update);

    // Set date for delta time
    lastUpdate = Date.now();

    newSprite = new PIXI.Sprite(gfx.actionPrompt);

    newSprite.anchor = { x: 0.5, y: 0.5 };
    newSprite.position = { x: 427, y: 240 };
    newSprite.scale = { x: 16, y: 16 };

    stage.addChild(newSprite);

};


// Performed on game update
game.update = function () {

    requestAnimFrame(game.update);

    // Calculate delta time and update lastUpdate
    var now = Date.now();
    var dt = (now - lastUpdate) / 1000;
    lastUpdate = now;

    // Actually render the stage
    renderer.render(stage);

};
