// chikun :: 2014
// Require libraries then run game


// Set default scaling mode
PIXI.scaleModes.DEFAULT = PIXI.scaleModes.NEAREST;

// Initialise important global variables
var stage, renderer, lastUpdate, gfx;

var libraries = [
    "js/game.js"
];


requirejs(libraries, function() {
    requirejs(["loadAssets"]);
});
